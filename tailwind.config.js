const colors = require('tailwindcss/colors')

module.exports = {
  // mode: "jit",
  separator: '_',
  purge: ['./**/*.{js,ts,jsx,tsx}'],
  darkMode: 'class',
  variants: {
    extend: {
      display: ['dark'],
    },
  },
  theme: {
    transparent: 'transparent',
    current: 'currentColor',
    ...colors,
  },
  plugins: [],
}
