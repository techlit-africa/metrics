const {
  override,
  disableEsLint,
  addBabelPlugins,
  addPostcssPlugins,
  addWebpackPlugin,
} = require('customize-cra')

const webpack = require('webpack')

module.exports = override(
  disableEsLint(),

  ...addBabelPlugins(
    'transform-jsx-classname-components',
    'transform-react-pug',
  ),

  addPostcssPlugins([
    require('autoprefixer'),
    require('tailwindcss'),
  ]),

  addWebpackPlugin(new webpack.ProvidePlugin({
    React: 'react',
    ErrorBoundary: ['react-error-boundary', 'ErrorBoundary'],
    Icon: ['@fortawesome/react-fontawesome', 'FontAwesomeIcon'],
  })),

  addWebpackPlugin(new webpack.DefinePlugin({
    NODE_ENV: `"${process.env.NODE_ENV || 'development'}"`,
    PDD_URL: `"${process.env.PDD_URL || 'http://localhost:1357'}"`,
    PDD_WS_URL: `"${process.env.PDD_WS_URL || 'ws://localhost:1357'}"`,
    METRICS_URL: `"${process.env.METRICS_URL || `http://localhost:${process.env.PORT || 4001}`}"`,
  })),
)
