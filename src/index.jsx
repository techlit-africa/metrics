import {render} from 'react-dom'
import {BrowserRouter} from 'react-router-dom'
import {SWRProvider} from 'lib/swr'
import {SocketProvider} from 'lib/socket'
import {App} from 'components/App'
import {syncLightsOn} from 'lib/theme'

import 'styles/index.css'
import 'init/fontawesome'
import 'init/dayjs'

const Main = () => {
  syncLightsOn()

  return pug`
    React.StrictMode
      BrowserRouter(basename='/metrics')
        SWRProvider
          SocketProvider
            App
  `
}

render(pug`Main`, document.getElementById('root'))
