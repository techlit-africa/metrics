import {library, config} from '@fortawesome/fontawesome-svg-core'

import {
  faLightbulb,
  faPlus,
  faTrashAlt,
  faSearch,
  faPen,
  faCircleNotch,
  faExclamationTriangle,
} from '@fortawesome/free-solid-svg-icons'

library.add(
  faLightbulb,
  faPlus,
  faTrashAlt,
  faSearch,
  faPen,
  faCircleNotch,
  faExclamationTriangle,
)

config.autoAddCss = false
