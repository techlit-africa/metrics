import mkCuid from 'cuid'
import {fetch} from 'lib/http'
import {toggleLightsOn} from 'lib/theme'
import useSWR, {mutate} from 'swr'
import fuzzysearch from 'fuzzysearch'
import dayjs from 'dayjs'

export const Home = () => pug`
  React.Suspense(fallback=${pug`Loading`})
    ErrorBoundary(fallback=${pug`Failed`})
      Ready
`

const Loading = () => pug`
  Layout
    .w-full.h-48.flex.items-center.justify-center.animate-spin
      Icon.text-5xl.text-green-400.dark_text-green-600(icon='circle-notch')
`

const Failed = () => pug`
  Layout
    .w-full.h-48.flex.items-center.justify-center.animate-pulse
      Icon.text-8xl.text-red-400.dark_text-red-600(icon='exclamation-triangle')
`

const Ready = () => {
  const [{
    searchValue,
    creatingSession,
    editingSession,
  }, update] = React.useReducer(
    (s1, s2) => ({...s1, ...s2}),
    {searchValue: '', creatingSession: false, editingSession: null},
  )

  console.log(creatingSession, editingSession)
  const closeForm = () => {
    console.log('closeForm')
    update({
      creatingSession: false,
      editingSession: null,
    })
  }

  return pug`
    Layout(
      searchValue=searchValue
      creatingSession=creatingSession
      editingSession=editingSession
      update=update
    )
      if creatingSession
        SessionForm(onClose=closeForm)
      if editingSession
        SessionForm(editingSession=editingSession onClose=closeForm)

      SessionList(searchValue=searchValue update=update)
  `
}

const Layout = ({
  searchValue,
  creatingSession,
  editingSession,
  update,
  children,
}) => pug`
  .w-full.h-full.min-h-screen.flex.flex-col.bg-gray-200.dark_bg-gray-800
    .w-full.h-12.px-4.flex.flex-row.items-center.justify-between.bg-gray-100.dark_bg-gray-900.border-b.border-gray-500.shadow
      h1.text-3xl.font-medium.text-gray-600.dark_text-gray-400 Metrics
      button(onClick=toggleLightsOn)
        Icon.text-xl.text-blue-600.dark_text-blue-400(icon='lightbulb')

    .w-full.flex-1.items-center.p-8
      .max-w-screen-md.m-auto.flex.flex-col.bg-gray-100.dark_bg-gray-900.border.border-gray-500.shadow
        .w-full.flex.flex-row.items-center.justify-between
          button.p-4.bg-blue-600.dark_bg-blue-400.text-white.dark_text-black
            Icon.text-xl(icon='search')
          input.flex-1.p-4.text-gray-800.dark_text-gray-200.font-medium.bg-gray-300.dark_bg-gray-700.outline-none.active_outline-none(
            placeholder='Search'
            value=searchValue
            onChange=(e)=>update({searchValue: e.target.value})
          )

          button.p-4.text-white.dark_text-black(
            className=(
              creatingSession || editingSession
                ? 'bg-gray-600 dark_bg-gray-400'
                : 'bg-blue-600 dark_bg-blue-400'
            )
            onClick=()=>update({creatingSession: true, editingSession: null})
            disabled=creatingSession || editingSession
          )
            span.pr-3.text-xl.leading-none.font-medium New Session
            Icon.text-xl(icon='plus')

        = children
`

const match = (normalizedNeedle, haystack) =>
  fuzzysearch(normalizedNeedle, haystack?.trim()?.toLowerCase() || '')

const SessionList = ({searchValue, update}) => {
  const {data: sessions} = useSWR('/metrics/sessions')

  const visibleSessions = React.useMemo(() => {
    return sessions.filter((s) => {
      const norm = searchValue.trim().toLowerCase()
      if (norm === '') return true

      if (match(norm, s.schoolName)) return true
      if (match(norm, s.streamName)) return true
      if (match(norm, s.notes)) return true
      if (match(norm, s.author)) return true

      return false
    })
  }, [sessions, searchValue])

  const sessionsByTimeAndPlace = React.useMemo(() => {
    let date = -1
    let schoolName = -1
    return visibleSessions.reduce((byTimeAndPlace, s) => {
      if (date === s.date && schoolName === s.schoolName) {
        byTimeAndPlace[byTimeAndPlace.length - 1].push(s)
      } else {
        date = s.date
        schoolName = s.schoolName
        byTimeAndPlace.push([s])
      }
      return byTimeAndPlace
    }, [])
  }, [visibleSessions])

  const editSession = (cuid) => {
    document.scrollingElement.scrollTo(0, 0)
    update({creatingSession: false, editingSession: cuid})
  }

  return pug`
    if sessions.length < 1
      .w-full.h-48.flex.items-center.justify-center
        h1.text-3xl.text-gray-500 No sessions recorded yet

    else if visibleSessions.length < 1
      .w-full.h-48.flex.items-center.justify-center
        h1.text-3xl.text-gray-500 No sessions match your search

    else
      for sessions in sessionsByTimeAndPlace
        .w-full.flex.flex-col.p-4(key=(sessions[0].schoolName || 'blank') + sessions[0].date)
          .flex.flex-row
            .pr-4.text-xl.text-gray-600.dark_text-gray-400.font-medium= sessions[0].schoolName || 'School blank'
            .text-gray-500.font-medium= sessions[0].date ? dayjs(sessions[0].date).format('dddd, D-M-YYYY') : ''

          for session in sessions
            .w-full.flex.flex-row.pt-4(key=session.cuid)
              .mx-4.border-r-4.border-gray-300.dark_border-gray-700
      
              .w-full.flex.flex-col.p-2.hover_bg-gray-200.dark_hover_bg-gray-800.cursor-pointer(
                onClick=()=>editSession(session.cuid)
              )
                .flex.flex-row.pb-4
                  .text-2xl.text-gray-900.dark_text-gray-100.font-medium.pr-2 Class #{session.className}
                  .text-2xl.text-gray-900.dark_text-gray-100.font-medium.pr-4= session.streamName
                  .text-lg.text-gray-500.pr-4 #{session.from} - #{session.until}
                  .text-sm.text-gray-500.pr-4 (#{session.students || 'N'} students)

                .flex.flex-row.whitespace-pre-wrap.text-gray-800.dark_text-gray-200= session.notes || 'Notes blank'
                .text-lg.text-gray-500 ~ #{session.author || 'Anonymous'}
  `
}

const SessionForm = ({editingSession, onClose}) => {
  const {data: sessions} = useSWR('/metrics/sessions')

  const isEditing = !!editingSession

  const init = React.useMemo(() => {
    return sessions.find((s) => s.cuid === editingSession) || {}
  }, [editingSession, sessions])

  const [{
    cuid,
    schoolName,
    date,
    className,
    streamName,
    from,
    until,
    students,
    notes,
    author,
    saving,
    error,
  }, update] = React.useReducer(
    (s1, s2) => ({...s1, ...s2}),
    {
      cuid: init.cuid || mkCuid(),
      schoolName: init.schoolName || sessions[0]?.schoolName || '',
      date: init.date || (new Date()).toISOString().slice(0, 10),
      className: init.className || '',
      streamName: init.streamName || '',
      from: init.from || '',
      until: init.until || '',
      students: init.students || '',
      notes: init.notes || '',
      author: init.author || '',
      saving: false,
      error: null,
    },
  )

  console.log('form', saving, error, editingSession)

  const save = () => {
    update({saving: true})

    fetch(isEditing ? `/metrics/sessions/${cuid}` : '/metrics/sessions', {
      method: isEditing ? 'PATCH' : 'POST',
      body: JSON.stringify({
        data: {
          cuid,
          schoolName,
          date,
          className,
          streamName,
          from,
          until,
          students,
          notes,
          author,
        },
      }),
    })
      .then(() => {
        mutate('/metrics/sessions')
        update({saving: false})
        onClose()
      })
      .catch((error) => {
        console.error(error)
        update({saving: false, error: true})
      })
  }

  const destroySession = () => {
    update({saving: true})

    fetch(`/metrics/sessions/${cuid}`, {method: 'DELETE'})
      .then(() => {
        mutate('/metrics/sessions')
        update({saving: false})
        onClose()
      })
      .catch((error) => {
        console.error(error)
        update({saving: false, error: true})
      })
  }

  return pug`
    .w-full.flex.flex-col.p-4
      .flex.flex-row
        .flex.flex-col.pr-4
          label.text-gray-500 School
          input.py-2.px-3.text-gray-800.dark_text-gray-200.bg-white.dark_bg-black.border.border-gray-300.dark_border-gray-700(
            disabled=saving
            placeholder='School'
            value=schoolName
            onChange=(e)=>update({schoolName: e.target.value})
          )
        .flex.flex-col.pr-4
          label.text-gray-500 Date
          input.py-2.px-3.text-gray-800.dark_text-gray-200.bg-white.dark_bg-black.border.border-gray-300.dark_border-gray-700(
            type='date'
            disabled=saving
            placeholder='Date'
            value=date
            onChange=(e)=>update({date: e.target.value})
          )

        .flex-1.flex.flex-row.justify-end
          if editingSession
            button.h-12.w-12.text-white.dark_text-black.bg-red-600.dark_bg-red-400(
              onClick=destroySession
              disabled=saving
            )
              Icon(icon='trash-alt')

      .w-full.flex.flex-row.py-4
        .ml-4.mr-8.border-r-4.border-gray-300.dark_border-gray-700

        .w-full.flex.flex-col
          .flex.flex-row.pb-4
            .flex.flex-col.pr-4
              label.text-gray-500 Class
              input.w-24.py-2.px-3.text-gray-800.dark_text-gray-200.bg-white.dark_bg-black.border.border-gray-300.dark_border-gray-700(
                disabled=saving
                placeholder='Class'
                value=className
                onChange=(e)=>update({className: e.target.value})
              )
            .flex.flex-col.pr-4
              label.text-gray-500 Stream
              input.w-24.py-2.px-3.text-gray-800.dark_text-gray-200.bg-white.dark_bg-black.border.border-gray-300.dark_border-gray-700(
                disabled=saving
                placeholder='Stream'
                value=streamName
                onChange=(e)=>update({streamName: e.target.value})
              )
            .flex.flex-col.pr-4
              label.text-gray-500 Students
              input.h-full.w-20.py-2.px-3.text-gray-800.dark_text-gray-200.bg-white.dark_bg-black.border.border-gray-300.dark_border-gray-700(
                disabled=saving
                placeholder='35'
                value=students
                onChange=(e)=>update({students: e.target.value})
              )
            .flex.flex-col.pr-4
              label.text-gray-500 From
              input.py-2.px-3.text-gray-800.dark_text-gray-200.bg-white.dark_bg-black.border.border-gray-300.dark_border-gray-700(
                type='time'
                value=from
                onChange=(e)=>update({from: e.target.value})
              )
            .flex.flex-col.pr-4
              label.text-gray-500 Until
              input.py-2.px-3.text-gray-800.dark_text-gray-200.bg-white.dark_bg-black.border.border-gray-300.dark_border-gray-700(
                type='time'
                value=until
                onChange=(e)=>update({until: e.target.value})
              )

          .w-full.flex.flex-col.pb-4
            label.text-gray-500 Author
            input.w-64.py-2.px-3.text-gray-800.dark_text-gray-200.bg-white.dark_bg-black.border.border-gray-300.dark_border-gray-700(
              disabled=saving
              placeholder='Your name'
              value=author
              onChange=(e)=>update({author: e.target.value})
            )

          .w-full.flex.flex-row.pb-8
            .w-full.flex.flex-col
              label.text-gray-500 Notes
              textarea.w-full.py-2.px-3.text-gray-800.dark_text-gray-200.bg-white.dark_bg-black.border.border-gray-300.dark_border-gray-700(
                rows=4
                disabled=saving
                placeholder='Today we did...'
                value=notes
                onChange=(e)=>update({notes: e.target.value})
              )

          .w-full.flex.flex-row
            button.flex-1.w-full.py-2.px-3.text-white.dark_text-black.bg-gray-500.dark_bg-gray-400(
              onClick=onClose
              disabled=saving
            ) Cancel

            .px-2

            button.flex-1.py-2.px-3.text-white.dark_text-black.bg-green-600.dark_bg-green-400(
              onClick=save
              disabled=saving
            ) Save
  `
}
