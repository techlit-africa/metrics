import {Switch, Route, Redirect} from 'react-router-dom'
import {Home} from 'components/Home'

export const App = () => {
  return pug`
    Switch
      Route(path='/') #[Home]
      Route #[Redirect(to='/')]
  `
}
